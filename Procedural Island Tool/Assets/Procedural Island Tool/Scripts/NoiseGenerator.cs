using UnityEngine;

public class NoiseGenerator : MonoBehaviour
{
    public float seed;
    bool alwaysGenerateNewSeed = true;
    int widthAndHeight = 256;
    [SerializeField] float offsetX;
    [SerializeField] float offsetY;

    [Space]

    public GameObject terrainObject;
    [SerializeField] GameObject noiseDisplayObject;

    [Space]

    [SerializeField] int depth = 20;
    [SerializeField] float scale = 20f;
    [SerializeField] float falloffRange = 100f;
    [SerializeField] float falloffPower = 10f;
    [SerializeField] float flatness = 2f;

    void Start ()
    {
        terrainObject.GetComponent<PaintTerrain> ().mapGeneratorObject = this.gameObject;
        // Generate Noise Texture - Start

        if(noiseDisplayObject != null)
        {
            Renderer renderer = noiseDisplayObject.GetComponent<Renderer> ();
            renderer.material.mainTexture = GenerateTexture ();
        }

        if (!alwaysGenerateNewSeed)
        {
            if (!PlayerPrefs.HasKey ("Seed"))
            {
                seed = PlayerPrefs.GetFloat ("Seed");
            }
            else
            {
                PlayerPrefs.SetFloat ("Seed", seed);
            }
        }

        offsetX += seed;
        offsetY += seed;

        // Generate Noise Texture - End
        // Edit Terrain - Start

        Terrain terrain = terrainObject.GetComponent<Terrain> ();
        terrain.terrainData = GenerateTerrain (terrain.terrainData);
        terrainObject.GetComponent<PaintTerrain> ().DrawTextures ();

        // Edit Terrain - End
    }

        // Generate Noise Texture - Start

    Texture2D GenerateTexture ()
    {
        Texture2D texture = new Texture2D (widthAndHeight, widthAndHeight);

        for (int x = 0; x < widthAndHeight; x++)
        {
            for (int y = 0; y < widthAndHeight; y++)
            {
                Color color = CalculateColor (x, y);
                texture.SetPixel (x, y, color);
            }
        }

        texture.Apply ();

        return texture;
    }

    Color CalculateColor (int x, int y)
    {
        float xCoord = (float)x / widthAndHeight * scale + offsetX;
        float yCoord = (float)y / widthAndHeight * scale + offsetY;
        float falloffFactor = FalloffFactor (x, y, widthAndHeight);

        float sample = Mathf.PerlinNoise (xCoord, yCoord);
        sample = sample / falloffFactor;
        return new Color (sample, sample, sample);
    }

    float FalloffFactor(int x, int y, int widthAndHeight)
    {
        float falloffFactor;
        float centerX = (float)widthAndHeight / 2;
        float centerY = (float)widthAndHeight / 2;

        float distanceFromCenter = Mathf.Sqrt(Mathf.Pow(((float)x - centerX), 2) + Mathf.Pow(((float)y - centerY), 2));
        falloffFactor = Mathf.Pow((Mathf.Pow((distanceFromCenter / falloffRange), falloffPower) + 1), 2);

        return falloffFactor;
    }

    // Generate Noise Texture - End
    // Generate Terrain - Start

    TerrainData GenerateTerrain (TerrainData terrainData)
    {
        terrainData.heightmapResolution = widthAndHeight + 1;
        terrainData.alphamapResolution = widthAndHeight + 1;

        terrainData.size = new Vector3 (widthAndHeight, depth, widthAndHeight);

        terrainData.SetHeights (0, 0, GenerateHeights ());

        return terrainData;
    }

    float [, ] GenerateHeights ()
    {
        float [, ] heights = new float [widthAndHeight, widthAndHeight];
        for (int x = 0; x < widthAndHeight; x++)
        {
            for (int y = 0; y < widthAndHeight; y++)
            {
                heights [x, y] = CalculateHeight (x, y);
                heights [x , y] = heights [x, y] / (flatness * FalloffFactor(x, y, widthAndHeight));
            }
        }

        return heights;
    }

    float CalculateHeight (int x, int y)
    {
        float xCoord = (float)x / widthAndHeight * scale + offsetX;
        float yCoord = (float)y / widthAndHeight * scale + offsetY;

        float sample = Mathf.PerlinNoise (xCoord, yCoord);
        return Mathf.PerlinNoise (sample, sample);
    }

    // Generate Terrain - End
}
