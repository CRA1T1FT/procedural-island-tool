using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintTerrain : MonoBehaviour
{
    [System.Serializable]
    public class SplatHeights
    {
        public int textureIndex;
        public int startingHeight;
    }

    public SplatHeights[] splatHeights;
    public GameObject mapGeneratorObject;

    public void DrawTextures ()
    {
        TerrainData terrainData = mapGeneratorObject.GetComponent<NoiseGenerator> ().terrainObject.GetComponent<Terrain> ().terrainData;
        float[, , ] splatmapData = new float [terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];

        for (int x = 0; x < terrainData.alphamapHeight; x++)
        {
            for (int y = 0; y < terrainData.alphamapWidth; y++)
            {
                float terrainHeight = terrainData.GetHeight (x, y);

                float[] splat = new float [splatHeights.Length];

                for (int i = 0; i < splatHeights.Length; i++)
                {
                    if (i == splatHeights.Length - 1 && terrainHeight >= splatHeights[i].startingHeight)
                    {
                        splat[i] = 1;
                    }
                    else if (terrainHeight >= splatHeights[i].startingHeight && terrainHeight <= splatHeights[i + 1].startingHeight)
                    {
                        splat[i] = 1;
                    }
                }

                for (int j = 0; j < splatHeights.Length; j++)
                {
                    splatmapData[y, x, j] = splat[j];
                }
            }
        }

        terrainData.SetAlphamaps (0, 0, splatmapData);
    }
}
